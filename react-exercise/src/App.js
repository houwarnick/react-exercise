import './App.css';
import Button from '@mui/material/Button';
import DataTable from 'react-data-table-component';
import React from 'react';
import Select from 'react-select'
import states from "./states.json";

const options = [
  { value: 'representatives', label: 'Representatives' },
  { value: 'senators', label: 'Senators' }
]

const columns = [
  {
      name: 'Name',
      selector: row => row.name,
      width: '200px',
  },
  {
      name: 'Party',
      selector: row => row.party,
      width: '200px',
  },
];

const ExpandedComponent = ({ data }) => 
  <div>
    <ul className="expandedList">
      <li>District: {data.district}</li>
      <li>Phone: {data.phone}</li>
      <li>Office: {data.office}</li>
      <li>Link: <a href={data.link}>{data.link}</a></li>
    </ul>
  </div>;

class App extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      state: null,
      type: null,
      button_disabled: true,
      table_data: []
    };
  }

  handleState(i) {
    this.setState({ state: i.value, button_disabled: this.state.type === null });
  }

  handleType(i) {
    this.setState({ type: i.value, button_disabled: this.state.state === null });
  }

  makeRequest() {
    const url = `http://127.0.0.1:3030/${this.state.type}/${this.state.state}`;
    fetch(url)
      .then((response) => response.json())
      .then((data) => {
        this.setState({table_data: data.results})
      })
  }

  render() {
    return (
      <div>
        <header>
          <h2>Who's My Representative </h2>
        </header>
        <div>
          <Select className="basic-single" options={options} onChange={(i) => this.handleType(i)} />
          <Select className="basic-single" options={states} onChange={(i) => this.handleState(i)} />
          <Button variant="contained" disabled={this.state.button_disabled} onClick={() => this.makeRequest()}>Search</Button>
          <DataTable
            columns={columns}
            data={this.state.table_data}
            expandableRows
            expandableRowsComponent={ExpandedComponent}
          />
        </div>
      </div>
    );
  }
}

export default App;
